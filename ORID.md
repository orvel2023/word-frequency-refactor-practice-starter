# O
  - Code Review : It deepens my understanding of the abstraction in object orientation and gives me a deeper understanding of the design of behavior in object orientation.
  - Design Mode : Today, I have learned three design patterns, namely observer pattern, strategy pattern and command pattern. I have a certain understanding of the application scenarios of each pattern, a detailed understanding of the code implementation of these patterns, and a distinction between the easily confused strategy pattern and command pattern.
  - Code Refactoring ： I have a certain understanding of various code smells, and know some common code smells and the order of code refactoring, which will be very helpful for refactoring my own code or others' code in the future.
  - Other understanding : Can design patterns be used for any scene? In fact, it is not, design patterns as a solution to a certain class of problems summed up the optimal solution, only when the problem is complex to a certain extent, the need to decouple and cohesion to consider the use of design patterns, otherwise it will only make more code complexity and performance costs and not benefit.
# R
  -  Helpful !
# I
  - I think today's tutorial will help me avoid writing code that smells like code. However, my acceptance of the design pattern is not high enough, and I still need to continue to learn deeply and deepen my mastery of the design pattern.
# D
  - Focus on code quality in future development and improve code quality through refactoring.
  - Learn more about the use of design patterns, and always remember not to abuse design patterns.
  - code review and refactor the code I wrote before.