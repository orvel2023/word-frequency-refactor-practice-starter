import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordFrequencyGame {
    private static final String CALCULATE_ERROR_MESSAGE = "Calculate Error";
    private static final String WORD_SEPARATOR = "\\s+";
    private static final String LINE_SEPARATOR = "\n";
    private static final String BLANK_SYMBOL = " ";

    public String calculateWordQuantity(String inputStr) {
        try {
            return  Stream.of(inputStr.split(WORD_SEPARATOR))
                    .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .sorted(Comparator.comparingLong(Map.Entry<String, Long>::getValue).reversed())
                    .map(entry -> entry.getKey() + BLANK_SYMBOL + entry.getValue())
                    .collect(Collectors.joining(LINE_SEPARATOR));
        } catch (Exception e) {
            return CALCULATE_ERROR_MESSAGE;
        }
    }
}
